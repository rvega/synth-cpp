#! /usr/bin/env bash

function iferr {
  echo "Error! Read compileVendors.sh and try compiling manually" 
  exit $1
}

echo "Compiling PortAudio" 
cd ../vendors/portaudio
./configure > /dev/null
iferr $?

