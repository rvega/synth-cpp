#include "ofMain.h"
#include "GUI.h"
#include "Messaging.h"

void GUI::setup(){
  ofSetBackgroundColor(30,30,30);
  ofSetColor(170,170,170);
  ofSetFrameRate(24);
}

void GUI::update(){
  // float rand = ofRandom(0, 1);
  // Messaging::sendFloatToDSP(ATTACK, rand);

  MessageType t;
  char v[STRING_MESSAGE_SIZE];
  while(Messaging::getStringInGUI(&t,&v[0])){
    std::cout << v << "\n";
  }

  Messaging::getAudioInGUI(audioSamples, 1600);
}

void GUI::draw(){
  int h = ofGetHeight();
  float v, v1;

  // Audio is interleaved stereo, draw only one channel
  for(int i=0; i<1598; i=i+2){
    v = audioSamples[i]; 
    v1 = audioSamples[i+2]; 
    ofDrawLine(i, 0.8*h/2*(1.25-v), i+1, 0.8*h/2*(1.25-v1));
  }
}
