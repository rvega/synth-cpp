#pragma once

class GUI{
  private:
    float audioSamples[4096];

  public:
    void setup();
    void update();
    void draw();
};
