#pragma once

#include <cassert>
#include <cstring>
#include "pa_ringbuffer.h"

#define MESSAGE_BUFFER_SIZE 128
#define AUDIO_BUFFER_SIZE 524288 // 96KHz * 2 sec * 2 channels, round to power of 2
#define STRING_MESSAGE_SIZE 32

/**
 * The message types. This is application specific
 */
enum MessageType{
  VOLUME=0,
  ATTACK,
  RELEASE,

  PRINT
};

/**
 * These datastructures are the messages actually being passed in the ring buffers
 */
class StringMessage{
  public:
    MessageType type;
    char value[STRING_MESSAGE_SIZE];
    StringMessage(MessageType t, const char* v): type(t){
      assert(strlen(v)<STRING_MESSAGE_SIZE && "Message too long");
      strncpy(&value[0], v, STRING_MESSAGE_SIZE);
    }
    StringMessage(): type(PRINT), value(""){}
};

class FloatMessage{
  public:
    MessageType type;
    float value;
    FloatMessage(): type(VOLUME), value(0){}
    FloatMessage(MessageType t, float v): type(t), value(v){}
};

/**
 * This class provides the public interface for messaging
 * and holds state and data.
 */
class Messaging{
  private:
    // Private constructor, no instances allowed.
    Messaging();

    // Ring buffer data space, sizes must be power of 2
    static FloatMessage rbFloatToDSPData[MESSAGE_BUFFER_SIZE];
    static StringMessage rbStringToGUIData[MESSAGE_BUFFER_SIZE];
    static float rbAudioToGUIData[AUDIO_BUFFER_SIZE];

    // The ring buffers
    static PaUtilRingBuffer rbFloatToDSP;
    static PaUtilRingBuffer rbStringToGUI;
    static PaUtilRingBuffer rbAudioToGUI;

  public:
    static void initRingBuffers();

    static inline void sendFloatToDSP(MessageType type, float value){
      FloatMessage msg(type, value);
      PaUtil_WriteRingBuffer(&rbFloatToDSP, &msg, 1);
    }

    static inline int getFloatInDSP(MessageType* t, float* f){
      FloatMessage msg;
      int count = PaUtil_ReadRingBuffer(&rbFloatToDSP, &msg, 1);
      *t = msg.type;
      *f = msg.value;
      return count;
    }

    static inline void sendStringToGUI(MessageType type, const char* value){
      StringMessage msg(type, value);
      PaUtil_WriteRingBuffer(&rbStringToGUI, &msg, 1);
    }

    static inline int getStringInGUI(MessageType* t, char* v){
      StringMessage msg;
      int count = PaUtil_ReadRingBuffer(&rbStringToGUI, &msg, 1);
      *t = msg.type;
      strncpy(v, msg.value, STRING_MESSAGE_SIZE);
      return count;
    }

    static inline void sendAudioToGUI(const float* audio, int samples){
      PaUtil_WriteRingBuffer(&rbAudioToGUI, audio, samples);
    }

    static inline int getAudioInGUI(float* audio, int count){
      int read = PaUtil_ReadRingBuffer(&rbAudioToGUI, audio, count);
      return read;
    }

    // Constructor
};

/**
 * Useful macros
 */
#ifndef NDEBUG
  #define DSP_DEBUG(format, ...) char msg[32]; \
      sprintf(msg, format, __VA_ARGS__); \
      Messaging::sendStringToGUI(PRINT, msg)
#else
  #define DSP_DEBUG(msg) (void(0))
#endif
  
