#include "App.h"
#include "Messaging.h"
#include <cassert>

// TODO: * Use RTAudio and JACK apis instead of OF (more control) and 
//         abstract it into an AudioIO class

void App::setup(){
  Messaging::initRingBuffers();
  
  audioSampleRate = 44100; // In Hertz
  controlSampleRate = audioSampleRate / 1000; // 1ms in number of audio samples
  ofSoundStreamSetup(2, 0, this, 44100, 256, 4);

  gui.setup();
}

void App::update(){
  gui.update();
}

void App::draw(){
  gui.draw();
}

void App::audioRequested(float * output, int bufferSize, int nChannels){
  float* out = output;
  for(int i=0; i<bufferSize; ++i){
    
    // Control rate
    if(i%controlSampleRate == 0){
      handleMessages();
    }

    // Audio rate
    float o = synth.tick();

    // Mono to stereo
    for(int c=0; c<nChannels; ++c){
      *output = o;
      output++;
    }
  }

  Messaging::sendAudioToGUI(out, bufferSize*nChannels);
}

// //--------------------------------------------------------------
// void App::keyPressed(int key){
//
// }
//
// //--------------------------------------------------------------
// void App::keyReleased(int key){
//
// }
//
// //--------------------------------------------------------------
// void App::mouseMoved(int x, int y ){
//
// }
//
// //--------------------------------------------------------------
// void App::mouseDragged(int x, int y, int button){
//
// }
//
// //--------------------------------------------------------------
// void App::mousePressed(int x, int y, int button){
//
// }
//
// //--------------------------------------------------------------
// void App::mouseReleased(int x, int y, int button){
//
// }
//
// //--------------------------------------------------------------
// void App::mouseEntered(int x, int y){
//
// }
//
// //--------------------------------------------------------------
// void App::mouseExited(int x, int y){
//
// }
//
// //--------------------------------------------------------------
// void App::windowResized(int w, int h){
//
// }
//
// //--------------------------------------------------------------
// void App::gotMessage(ofMessage msg){
//
// }
//
// //--------------------------------------------------------------
// void App::dragEvent(ofDragInfo dragInfo){ 
//
// }
