#include "Messaging.h"
#include <string>
#include <iostream>

// Initialize static members
FloatMessage Messaging::rbFloatToDSPData[MESSAGE_BUFFER_SIZE];
StringMessage Messaging::rbStringToGUIData[MESSAGE_BUFFER_SIZE];
float Messaging::rbAudioToGUIData[AUDIO_BUFFER_SIZE];
PaUtilRingBuffer Messaging::rbFloatToDSP;
PaUtilRingBuffer Messaging::rbStringToGUI;
PaUtilRingBuffer Messaging::rbAudioToGUI;

void Messaging::initRingBuffers(){
  PaUtil_InitializeRingBuffer(&rbFloatToDSP, sizeof(FloatMessage), MESSAGE_BUFFER_SIZE, &rbFloatToDSPData);
  PaUtil_InitializeRingBuffer(&rbStringToGUI, sizeof(StringMessage), MESSAGE_BUFFER_SIZE, &rbStringToGUIData);
  PaUtil_InitializeRingBuffer(&rbAudioToGUI, sizeof(float), AUDIO_BUFFER_SIZE, &rbAudioToGUIData);
}

