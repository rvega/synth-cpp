#pragma once

#include <cmath>

class Synth{
  private: 
    float frequency;
    int t;

  public:
    Synth();
    void setFrequency(float f);

    inline float tick(){
      const int sr = 44100;
      const float pi = std::acos(-1);
      t++;
      return std::sin(2*pi*frequency*t/sr);
    }
};
