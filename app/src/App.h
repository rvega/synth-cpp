#pragma once

#include "ofMain.h"
#include "Synth.h"
#include "GUI.h"

class App : public ofBaseApp{

  private:
    int audioSampleRate;
    int controlSampleRate;
    void handleMessages();
    Synth synth;
    GUI gui;

  public:
    void setup();
    void update();
    void draw();
    void audioRequested(float * output, int bufferSize, int nChannels);

    // void keyPressed(int key);
    // void keyReleased(int key);
    // void mouseMoved(int x, int y );
    // void mouseDragged(int x, int y, int button);
    // void mousePressed(int x, int y, int button);
    // void mouseReleased(int x, int y, int button);
    // void mouseEntered(int x, int y);
    // void mouseExited(int x, int y);
    // void windowResized(int w, int h);
    // void dragEvent(ofDragInfo dragInfo);
    // void gotMessage(ofMessage msg);

};
